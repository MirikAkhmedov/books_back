<?php

namespace Tests\Feature\Books;

use App\Models\Book;
use Tests\TestCase;

class StoreBookTest extends TestCase
{
    public function test_store_a_book()
    {
        $book = Book::factory()->make()->toArray();

        $this->postJson(route('books.store', $book))->assertCreated();

        $this->assertDatabaseHas('books', $book);
    }
}
