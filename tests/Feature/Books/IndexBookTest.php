<?php

namespace Tests\Feature\Books;

use App\Models\Book;
use Tests\TestCase;

class IndexBookTest extends TestCase
{
    public function test_get_all_books()
    {
        $book = Book::all();

        $this->getJson(route('books.index'))
            ->assertSee($book->first()->name)
            ->assertOk();
    }

    public function test_get_a_book()
    {
        $book = Book::first();

        $this->getJson(route('books.show', $book->id))->assertOk()->assertSeeText($book->name);
    }
}
