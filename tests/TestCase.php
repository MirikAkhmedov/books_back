<?php

namespace Tests;

use Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Storage;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        /**
         * Enable exception handling
         */
        $this->withExceptionHandling();

        /**
         * Seed necessary data
         */
        Artisan::call('db:seed');

        /**
         * Create a temporary storage path for testing
         */
        Storage::fake();
    }

}
