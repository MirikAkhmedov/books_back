<?php

namespace App\Models;

use App\Models\Relationships\CommentRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasFactory, CommentRelationships;

    protected static $unguarded = true;

    /**
     * Relationship between book and comments
     *
     * @return BelongsTo
     */
}
