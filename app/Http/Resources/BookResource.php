<?php

namespace App\Http\Resources;

use App\Models\Book;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @mixin Book
 */
class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'price'      => $this->price,
            'free'       => $this->free,
            'image'      => $this->image,
            'likes'      => $this->likes,
            'views'      => $this->views,
            'comments'   => $this->comments,
            'created_at' => $this->created_at,
        ];
    }
}
